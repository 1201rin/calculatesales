package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "支店定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "支店定義ファイルのフォーマットが不正です";
	private static final String FILE_NOT_NUMBER = "売り上げファイルが連番になっていません";
	private static final String ERROR_TOTAL_MONEY = "合計金額が10桁を超えました";
	private static final String FILE_NOT_CODE = "の支店コードが不正です";
	private static final String FILE_NOT_FORMAT = "のフォーマットが不正です";
	
	
	
	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		//コマンドライン引数が一つかの確認
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();


		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales)) {
			return;
		}
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		
		for(int i = 0; i < files.length; i++) {
			//対象がファイルなのか、数字8桁.rcdなのか確認
			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}\\.rcd$")) {
				rcdFiles.add(files[i]);

			}

		}
		
		Collections.sort(rcdFiles);
		
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));
			
			if((latter - former) != 1) {
				System.out.println(FILE_NOT_NUMBER);
				return;
			}
		}
		
		for(int i = 0; i < rcdFiles.size(); i++) {
			BufferedReader br = null;
			try {
				FileReader fr = new FileReader (rcdFiles.get(i));
				br = new BufferedReader(fr);

				String line;
				//1行ずつ読み込む ファイル内で改行しているので新たにリストを作る
				List<String> sales = new ArrayList<>();
				while((line = br.readLine()) != null){
					sales.add(line);
				}
				//フォーマットの確認
				if(sales.size() != 2) {
					System.out.println(rcdFiles.get(i).getName() + FILE_NOT_FORMAT);
					return;
				}
				//コードがファイルにあるかの確認
				if(!branchSales.containsKey(sales.get(0))) {
					System.out.println(rcdFiles.get(i).getName() +  FILE_NOT_CODE);
					return;
				}
				//売上金額が数字か確認
				if(!sales.get(1).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}
				//型変換
				long fileSale = Long.parseLong(sales.get(1));
				Long saleAmount = branchSales.get(sales.get(0)) + fileSale;
				
				//指定した桁数を超えていないかの確認
				if(saleAmount >= 10000000000L) {
					System.out.println(ERROR_TOTAL_MONEY);
					return;
				}
				
				branchSales.put(sales.get(0),saleAmount);
				

			}catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> branchNames, Map<String, Long> branchSales) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			if(!file.exists()) {
				System.out.println(FILE_NOT_EXIST);
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");
				
				if((items.length != 2) || (!items[0].matches("^[0-9]{3}$"))) {
					System.out.println(FILE_INVALID_FORMAT);
					return false;
				}
				
				branchNames.put(items[0],items[1]);
				branchSales.put(items[0], 0L);
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> branchNames, Map<String, Long> branchSales) {
		
		BufferedWriter bw = null;
		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for(String key:  branchNames.keySet()) {
				String value = branchNames.get(key);
				 bw.write(key + "," + value + "," + branchSales.get(key));
				 bw.newLine();
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}

